/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
};

module.exports = nextConfig;

// next.config.js
module.exports = {
  images: {
    domains: ["images.prismic.io", "images.unsplash.com"],
    formats: ["image/avif", "image/webp"],
  },
};
