import { createClient } from "../../prismicio";
import { GetStaticPropsContext, InferGetStaticPropsType } from "next";
import s from "./blogPage.module.css";
import { PrismicRichText } from "@prismicio/react";
import Image from "next/image";
import React from "react";

export async function getStaticProps({
  params,
  previewData,
}: GetStaticPropsContext) {
  const client = createClient({ previewData });

  const page = await client.getByUID("blog_page", String(params?.slug));

  return {
    props: {
      page,
    },
  };
}

export async function getStaticPaths() {
  const client = createClient();

  const pages = await client.getAllByType("blog_page");

  return {
    paths: pages.map((p: any) => ({ params: { slug: p.uid } })),
    fallback: false,
  };
}

export default function BlogPost({
  page,
}: InferGetStaticPropsType<typeof getStaticProps>) {
  console.log("blog", page);
  return (
    <section className={s.blog}>
      {page.data.title && (
        <span className={s.title}>
          <PrismicRichText field={page.data.title} />
        </span>
      )}
      {page.data.image?.url && (
        <div className={s.imageWrapper}>
          <Image
            src={page.data.image.url}
            alt={page.data.image.alt}
            layout="fill"
            objectFit="cover"
          />
        </div>
      )}
      {page.data.body && (
        <div className={s.bodyWrapper}>
          <PrismicRichText field={page.data.body} />
        </div>
      )}
    </section>
  );
}
