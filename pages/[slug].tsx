import s from "./CMSPage.module.css";
import { GetStaticPropsContext, InferGetStaticPropsType } from "next";
import { createClient } from "../prismicio";
import { SliceZone } from "@prismicio/react";
import { components } from "../slices";
import { PrismicRichText } from "@prismicio/react";

export async function getStaticProps({
  params,
  previewData,
}: GetStaticPropsContext) {
  const client = createClient({ previewData });

  const page = await client.getByUID("cms_page", String(params?.slug));

  return {
    props: {
      page,
    },
  };
}

export async function getStaticPaths() {
  const client = createClient();

  const pages = await client.getAllByType("cms_page");

  return {
    paths: pages.map((p: any) => ({ params: { slug: p.uid } })),
    fallback: false,
  };
}

export default function CMSPage({
  page,
}: InferGetStaticPropsType<typeof getStaticProps>) {
  console.log("Page", page);
  return (
    <section className={s.root}>
      {page.data.title && (
        <div className={s.titleSection}>
          <PrismicRichText field={page.data.title} />
        </div>
      )}
      <SliceZone slices={page.data.slices} components={components} />
    </section>
  );
}
