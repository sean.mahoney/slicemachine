import "../styles/globals.css";
import type { AppProps } from "next/app";
import Link from "next/link";
import { PrismicProvider } from "@prismicio/react";
import { PrismicPreview } from "@prismicio/next";
import { repositoryName } from "../prismicio";
import DesktopNavigation from "../common/DesktopNavigation";
import DesktopFooter from "../common/DesktopFooter";

export default function App({ Component, pageProps }: AppProps) {
  const navigationLinks = [
    { text: "Home", link: "/" },
    { text: "About", link: "/about" },
    { text: "Blog", link: "/blog" },
  ];
  return (
    <PrismicProvider internalLinkComponent={(props) => <Link {...props} />}>
      <PrismicPreview repositoryName={repositoryName}>
        <DesktopNavigation navigationLinks={navigationLinks} />
        <Component {...pageProps} />
        <DesktopFooter navigationLinks={navigationLinks} />
      </PrismicPreview>
    </PrismicProvider>
  );
}
