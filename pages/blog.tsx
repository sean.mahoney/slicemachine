import s from "./BlogList.module.css";
import { GetStaticPropsContext, InferGetStaticPropsType } from "next";
import { createClient } from "../prismicio";
import { PrismicRichText } from "@prismicio/react";
import Image from "next/image";
import Link from "next/link";

export async function getStaticProps({ previewData }: GetStaticPropsContext) {
  const client = createClient({ previewData });

  const page = await client.getSingle("blog_list");
  const blogList = await client.getAllByType("blog_page");

  return {
    props: {
      page,
      blogList,
    },
  };
}

export default function Blog({
  page,
  blogList,
}: InferGetStaticPropsType<typeof getStaticProps>) {
  return (
    <section className={s.root}>
      <div className={s.titleSection}>
        {page.data.title && (
          <span className={s.title}>
            <PrismicRichText field={page.data.title} />
          </span>
        )}
        {page.data.description && (
          <PrismicRichText field={page.data.description} />
        )}
      </div>
      <div className={s.blogList}>
        {blogList?.map((item, i) => (
          <div key={i} className={s.blogListItem}>
            <Link href={`/blog/${item.uid}`}>
              {item.data.title && <PrismicRichText field={item.data.title} />}
              {item.data.blog_list_image.url && (
                <div className={s.imageWrapper}>
                  <Image
                    src={item.data.blog_list_image.url}
                    alt={
                      item.data.blog_list_image.alt &&
                      item.data.blog_list_image.alt
                    }
                    layout="fill"
                    objectFit="cover"
                  />
                </div>
              )}
              {item.data.short_description && (
                <PrismicRichText field={item.data.short_description} />
              )}
            </Link>
          </div>
        ))}
      </div>
    </section>
  );
}
