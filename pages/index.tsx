import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { SliceZone } from '@prismicio/react'
import { createClient } from '../prismicio'
import { components } from '../slices'
import {GetStaticPropsContext, InferGetStaticPropsType} from "next";

export async function getStaticProps({ previewData }: GetStaticPropsContext) {
  const client = createClient({ previewData })

  const page = await client.getSingle('homepage')

  return {
    props: {
      page,
    },
  }
}

export default function Home({page}: InferGetStaticPropsType<typeof getStaticProps>) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Digital Six - Prismic Slice Machine</title>
        <meta name="description" content="Digital Six Tries Prismic Slice Machine" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SliceZone slices={page.data.slices} components={components} />

    </div>
  )
}
