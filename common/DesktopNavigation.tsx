import Link from "next/link";
import s from "./DesktopNavigation.module.css";
import Image from "next/image";

interface Props {
  navigationLinks: { text: string; link: string }[];
}

export default function DesktopNavigation({ navigationLinks }: Props) {
  return (
    <section className={s.root}>
      <Image src={"/digital-six.png"} alt={"Logo"} width={157} height={27} />
      <ul className={s.desktopLinks}>
        {navigationLinks.map((l: any, index: number) => (
          <li key={index}>
            <Link href={l.link}>{l.text}</Link>
          </li>
        ))}
      </ul>
    </section>
  );
}
