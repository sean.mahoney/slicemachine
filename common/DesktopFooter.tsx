import s from "./DesktopFooter.module.css";
import Link from "next/link";

interface Props {
  navigationLinks: { text: string; link: string }[];
}

export default function DesktopFooter({ navigationLinks }: Props) {
  return (
    <section className={s.root}>
      <h2>Digital Six</h2>
      <ul className={s.desktopLinks}>
        {navigationLinks.map((l: any, index: number) => (
          <li key={index}>
            <Link href={l.link}>{l.text}</Link>
          </li>
        ))}
      </ul>
    </section>
  );
}
