import React from "react";
import { PrismicRichText } from "@prismicio/react";
import s from "./DoubleColumnText.module.css";

/**
 * @typedef {import("@prismicio/client").Content.DoubleColumnTextSlice} DoubleColumnTextSlice
 * @typedef {import("@prismicio/react").SliceComponentProps<DoubleColumnTextSlice>} DoubleColumnTextProps
 * @param { DoubleColumnTextProps }
 */
const DoubleColumnText = ({ slice }) => (
  <section className={s.root}>
    <div className={s.textWrapper}>
      <span className={s.title}>
        {slice.primary.title ? (
          <PrismicRichText field={slice.primary.title} />
        ) : (
          <h2>Template slice, update me!</h2>
        )}
      </span>
      <div className={s.columns}>
        {slice.primary.description ? (
          <PrismicRichText field={slice.primary.description} />
        ) : (
          <p>start by editing this slice from inside Slice Machine!</p>
        )}
      </div>
    </div>
  </section>
);

export default DoubleColumnText;
