import React from "react";
import { PrismicRichText } from "@prismicio/react";
import s from "./CenteredText.module.css";

/**
 * @typedef {import("@prismicio/client").Content.CenteredTextSlice} CenteredTextSlice
 * @typedef {import("@prismicio/react").SliceComponentProps<CenteredTextSlice>} CenteredTextProps
 * @param { CenteredTextProps }
 */
const CenteredText = ({ slice }) => (
  <section className={s.root}>
    <span className={s.title}>
      {slice.primary.title ? (
        <PrismicRichText field={slice.primary.title} />
      ) : (
        <h2>Template slice, update me!</h2>
      )}
    </span>
    {slice.primary.description ? (
      <PrismicRichText field={slice.primary.description} />
    ) : (
      <p>start by editing this slice from inside Slice Machine!</p>
    )}
  </section>
);

export default CenteredText;
