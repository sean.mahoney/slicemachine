import React from "react";
import { PrismicRichText } from "@prismicio/react";
import s from "./SingleColumnText.module.css";

/**
 * @typedef {import("@prismicio/client").Content.SingleColumnTextSlice} SingleColumnTextSlice
 * @typedef {import("@prismicio/react").SliceComponentProps<SingleColumnTextSlice>} SingleColumnTextProps
 * @param { SingleColumnTextProps }
 */
const SingleColumnText = ({ slice }) => (
  <section className={s.root}>
    <div className={s.textWrapper}>
      <span className={s.title}>
        {slice.primary.title ? (
          <PrismicRichText field={slice.primary.title} />
        ) : (
          <h2>Template slice, update me!</h2>
        )}
      </span>
      {slice.primary.description ? (
        <PrismicRichText field={slice.primary.description} />
      ) : (
        <p>start by editing this slice from inside Slice Machine!</p>
      )}
    </div>
  </section>
);

export default SingleColumnText;
