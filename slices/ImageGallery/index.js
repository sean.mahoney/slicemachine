import React from "react";
import { PrismicRichText } from "@prismicio/react";
import s from "./ImageGallery.module.css";
import Image from "next/image";

/**
 * @typedef {import("@prismicio/client").Content.ImageGallerySlice} ImageGallerySlice
 * @typedef {import("@prismicio/react").SliceComponentProps<ImageGallerySlice>} ImageGalleryProps
 * @param { ImageGalleryProps }
 */
const ImageGallery = ({ slice }) => (
  <section className={s.root}>
    <span className={s.title}>
      {slice.primary.title ? (
        <PrismicRichText field={slice.primary.title} />
      ) : (
        <h2>Template slice, update me!</h2>
      )}
    </span>
    <div className={s.gallery}>
      {slice?.items?.map((item, i) => (
        <div key={i} className={s.imageWrapper}>
          <Image
            src={item.gallery_image.url}
            alt={item.gallery_image.alt}
            layout="fill"
            objectFit="cover"
          />
        </div>
      ))}
    </div>
  </section>
);

export default ImageGallery;
