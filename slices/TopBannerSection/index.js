import React from "react";
import { PrismicRichText } from "@prismicio/react";
import s from "./TopBannerSection.module.css";

/**
 * @typedef {import("@prismicio/client").Content.TopBannerSectionSlice} TopBannerSectionSlice
 * @typedef {import("@prismicio/react").SliceComponentProps<TopBannerSectionSlice>} TopBannerSectionProps
 * @param { TopBannerSectionProps }
 */
const TopBannerSection = ({ slice }) => (
  <section
    className={s.root}
    style={{
      backgroundImage: slice.primary.background_image?.url
        ? `url(${slice.primary.background_image.url})`
        : "unset",
    }}
  >
    <div className={s.contentWrapper}>
      <span className={s.title}>
        {slice.primary.title ? (
          <PrismicRichText field={slice.primary.title} />
        ) : (
          <h2>Template slice, update me!</h2>
        )}
      </span>
      {slice.primary.body ? (
        <PrismicRichText field={slice.primary.body} />
      ) : (
        <p>start by editing this slice from inside Slice Machine!</p>
      )}
    </div>
  </section>
);

export default TopBannerSection;
