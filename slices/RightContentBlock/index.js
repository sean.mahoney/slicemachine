import React from "react";
import { PrismicRichText } from "@prismicio/react";
import Image from "next/image";
import s from "./RightContentBlock.module.css";

/**
 * @typedef {import("@prismicio/client").Content.RightContentBlockSlice} RightContentBlockSlice
 * @typedef {import("@prismicio/react").SliceComponentProps<RightContentBlockSlice>} RightContentBlockProps
 * @param { RightContentBlockProps }
 */
const RightContentBlock = ({ slice }) => (
  <section className={s.root}>
    <div className={s.contentWrapper}>
      <span className={s.title}>
        {slice.primary.title ? (
          <PrismicRichText field={slice.primary.title} />
        ) : (
          <h2>Template slice, update me!</h2>
        )}
      </span>
      {slice.primary.description ? (
        <PrismicRichText field={slice.primary.description} />
      ) : (
        <p>start by editing this slice from inside Slice Machine!</p>
      )}
    </div>
    {slice.primary.image?.url && (
      <div className={s.imageWrapper}>
        <Image
          src={slice.primary.image.url}
          alt={slice.primary.image.alt}
          layout="fill"
          objectFit="cover"
        />
      </div>
    )}
  </section>
);

export default RightContentBlock;
