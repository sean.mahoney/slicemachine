import React from "react";
import { PrismicRichText } from "@prismicio/react";
import s from "./LeftContentBlock.module.css";
import Image from "next/image";

/**
 * @typedef {import("@prismicio/client").Content.LeftContentBlockSlice} LeftContentBlockSlice
 * @typedef {import("@prismicio/react").SliceComponentProps<LeftContentBlockSlice>} LeftContentBlockProps
 * @param { LeftContentBlockProps }
 */
const LeftContentBlock = ({ slice }) => (
  <section className={s.root}>
    {slice.primary.image?.url && (
      <div className={s.imageWrapper}>
        <Image
          src={slice.primary.image.url}
          alt={slice.primary.image.alt}
          layout="fill"
          objectFit="cover"
        />
      </div>
    )}
    <div className={s.contentWrapper}>
      <span className={s.title}>
        {slice.primary.title ? (
          <PrismicRichText field={slice.primary.title} />
        ) : (
          <h2>Template slice, update me!</h2>
        )}
      </span>
      {slice.primary.description ? (
        <PrismicRichText field={slice.primary.description} />
      ) : (
        <p>start by editing this slice from inside Slice Machine!</p>
      )}
    </div>
  </section>
);

export default LeftContentBlock;
