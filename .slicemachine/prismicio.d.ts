// Code generated by Slice Machine. DO NOT EDIT.

import type * as prismicT from "@prismicio/types";
import type * as prismic from "@prismicio/client";

type Simplify<T> = {
    [KeyType in keyof T]: T[KeyType];
};
/** Content for Blog List documents */
interface BlogListDocumentData {
    /**
     * title field in *Blog List*
     *
     * - **Field Type**: Title
     * - **Placeholder**: *None*
     * - **API ID Path**: blog_list.title
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    title: prismicT.TitleField;
    /**
     * description field in *Blog List*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: *None*
     * - **API ID Path**: blog_list.description
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    description: prismicT.RichTextField;
}
/**
 * Blog List document from Prismic
 *
 * - **API ID**: `blog_list`
 * - **Repeatable**: `false`
 * - **Documentation**: https://prismic.io/docs/core-concepts/custom-types
 *
 * @typeParam Lang - Language API ID of the document.
 */
export type BlogListDocument<Lang extends string = string> = prismicT.PrismicDocumentWithoutUID<Simplify<BlogListDocumentData>, "blog_list", Lang>;
/** Content for Blog Page documents */
interface BlogPageDocumentData {
    /**
     * Blog List Image field in *Blog Page*
     *
     * - **Field Type**: Image
     * - **Placeholder**: *None*
     * - **API ID Path**: blog_page.blog_list_image
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/image
     *
     */
    blog_list_image: prismicT.ImageField<never>;
    /**
     * Short Description field in *Blog Page*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: *None*
     * - **API ID Path**: blog_page.short_description
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    short_description: prismicT.RichTextField;
    /**
     * Blog Title field in *Blog Page*
     *
     * - **Field Type**: Title
     * - **Placeholder**: *None*
     * - **API ID Path**: blog_page.title
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    title: prismicT.TitleField;
    /**
     * image field in *Blog Page*
     *
     * - **Field Type**: Image
     * - **Placeholder**: *None*
     * - **API ID Path**: blog_page.image
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/image
     *
     */
    image: prismicT.ImageField<never>;
    /**
     * body field in *Blog Page*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: *None*
     * - **API ID Path**: blog_page.body
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    body: prismicT.RichTextField;
    /**
     * Slice Zone field in *Blog Page*
     *
     * - **Field Type**: Slice Zone
     * - **Placeholder**: *None*
     * - **API ID Path**: blog_page.slices[]
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/slices
     *
     */
    slices: prismicT.SliceZone<BlogPageDocumentDataSlicesSlice>;
}
/**
 * Slice for *Blog Page → Slice Zone*
 *
 */
type BlogPageDocumentDataSlicesSlice = never;
/**
 * Blog Page document from Prismic
 *
 * - **API ID**: `blog_page`
 * - **Repeatable**: `true`
 * - **Documentation**: https://prismic.io/docs/core-concepts/custom-types
 *
 * @typeParam Lang - Language API ID of the document.
 */
export type BlogPageDocument<Lang extends string = string> = prismicT.PrismicDocumentWithUID<Simplify<BlogPageDocumentData>, "blog_page", Lang>;
/** Content for CMS Page documents */
interface CmsPageDocumentData {
    /**
     * title field in *CMS Page*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: *None*
     * - **API ID Path**: cms_page.title
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    title: prismicT.RichTextField;
    /**
     * Slice Zone field in *CMS Page*
     *
     * - **Field Type**: Slice Zone
     * - **Placeholder**: *None*
     * - **API ID Path**: cms_page.slices[]
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/slices
     *
     */
    slices: prismicT.SliceZone<CmsPageDocumentDataSlicesSlice>;
}
/**
 * Slice for *CMS Page → Slice Zone*
 *
 */
type CmsPageDocumentDataSlicesSlice = CenteredTextSlice | ImageGallerySlice | LeftContentBlockSlice | RightContentBlockSlice | TopBannerSectionSlice | SingleColumnTextSlice | DoubleColumnTextSlice;
/**
 * CMS Page document from Prismic
 *
 * - **API ID**: `cms_page`
 * - **Repeatable**: `true`
 * - **Documentation**: https://prismic.io/docs/core-concepts/custom-types
 *
 * @typeParam Lang - Language API ID of the document.
 */
export type CmsPageDocument<Lang extends string = string> = prismicT.PrismicDocumentWithUID<Simplify<CmsPageDocumentData>, "cms_page", Lang>;
/** Content for Homepage documents */
interface HomepageDocumentData {
    /**
     * Meta Title field in *Homepage*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: *None*
     * - **API ID Path**: homepage.meta_title
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    meta_title: prismicT.RichTextField;
    /**
     * Meta Description field in *Homepage*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: *None*
     * - **API ID Path**: homepage.meta_description
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    meta_description: prismicT.RichTextField;
    /**
     * Slice Zone field in *Homepage*
     *
     * - **Field Type**: Slice Zone
     * - **Placeholder**: *None*
     * - **API ID Path**: homepage.slices[]
     * - **Tab**: Main
     * - **Documentation**: https://prismic.io/docs/core-concepts/slices
     *
     */
    slices: prismicT.SliceZone<HomepageDocumentDataSlicesSlice>;
}
/**
 * Slice for *Homepage → Slice Zone*
 *
 */
type HomepageDocumentDataSlicesSlice = TopBannerSectionSlice | LeftContentBlockSlice | RightContentBlockSlice | CenteredTextSlice | ImageGallerySlice;
/**
 * Homepage document from Prismic
 *
 * - **API ID**: `homepage`
 * - **Repeatable**: `false`
 * - **Documentation**: https://prismic.io/docs/core-concepts/custom-types
 *
 * @typeParam Lang - Language API ID of the document.
 */
export type HomepageDocument<Lang extends string = string> = prismicT.PrismicDocumentWithoutUID<Simplify<HomepageDocumentData>, "homepage", Lang>;
export type AllDocumentTypes = BlogListDocument | BlogPageDocument | CmsPageDocument | HomepageDocument;
/**
 * Primary content in CenteredText → Primary
 *
 */
interface CenteredTextSliceDefaultPrimary {
    /**
     * Title field in *CenteredText → Primary*
     *
     * - **Field Type**: Title
     * - **Placeholder**: This is where it all begins...
     * - **API ID Path**: centered_text.primary.title
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    title: prismicT.TitleField;
    /**
     * Description field in *CenteredText → Primary*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: A nice description of your feature
     * - **API ID Path**: centered_text.primary.description
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    description: prismicT.RichTextField;
}
/**
 * Default variation for CenteredText Slice
 *
 * - **API ID**: `default`
 * - **Description**: `CenteredText`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type CenteredTextSliceDefault = prismicT.SharedSliceVariation<"default", Simplify<CenteredTextSliceDefaultPrimary>, never>;
/**
 * Slice variation for *CenteredText*
 *
 */
type CenteredTextSliceVariation = CenteredTextSliceDefault;
/**
 * CenteredText Shared Slice
 *
 * - **API ID**: `centered_text`
 * - **Description**: `CenteredText`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type CenteredTextSlice = prismicT.SharedSlice<"centered_text", CenteredTextSliceVariation>;
/**
 * Primary content in DoubleColumnText → Primary
 *
 */
interface DoubleColumnTextSliceDefaultPrimary {
    /**
     * Title field in *DoubleColumnText → Primary*
     *
     * - **Field Type**: Title
     * - **Placeholder**: This is where it all begins...
     * - **API ID Path**: double_column_text.primary.title
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    title: prismicT.TitleField;
    /**
     * Description field in *DoubleColumnText → Primary*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: A nice description of your feature
     * - **API ID Path**: double_column_text.primary.description
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    description: prismicT.RichTextField;
}
/**
 * Default variation for DoubleColumnText Slice
 *
 * - **API ID**: `default`
 * - **Description**: `DoubleColumnText`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type DoubleColumnTextSliceDefault = prismicT.SharedSliceVariation<"default", Simplify<DoubleColumnTextSliceDefaultPrimary>, never>;
/**
 * Slice variation for *DoubleColumnText*
 *
 */
type DoubleColumnTextSliceVariation = DoubleColumnTextSliceDefault;
/**
 * DoubleColumnText Shared Slice
 *
 * - **API ID**: `double_column_text`
 * - **Description**: `DoubleColumnText`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type DoubleColumnTextSlice = prismicT.SharedSlice<"double_column_text", DoubleColumnTextSliceVariation>;
/**
 * Primary content in ImageGallery → Primary
 *
 */
interface ImageGallerySliceDefaultPrimary {
    /**
     * Title field in *ImageGallery → Primary*
     *
     * - **Field Type**: Title
     * - **Placeholder**: This is where it all begins...
     * - **API ID Path**: image_gallery.primary.title
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    title: prismicT.TitleField;
}
/**
 * Item in ImageGallery → Items
 *
 */
export interface ImageGallerySliceDefaultItem {
    /**
     * Gallery Image field in *ImageGallery → Items*
     *
     * - **Field Type**: Image
     * - **Placeholder**: *None*
     * - **API ID Path**: image_gallery.items[].gallery_image
     * - **Documentation**: https://prismic.io/docs/core-concepts/image
     *
     */
    gallery_image: prismicT.ImageField<never>;
}
/**
 * Default variation for ImageGallery Slice
 *
 * - **API ID**: `default`
 * - **Description**: `ImageGallery`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type ImageGallerySliceDefault = prismicT.SharedSliceVariation<"default", Simplify<ImageGallerySliceDefaultPrimary>, Simplify<ImageGallerySliceDefaultItem>>;
/**
 * Slice variation for *ImageGallery*
 *
 */
type ImageGallerySliceVariation = ImageGallerySliceDefault;
/**
 * ImageGallery Shared Slice
 *
 * - **API ID**: `image_gallery`
 * - **Description**: `ImageGallery`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type ImageGallerySlice = prismicT.SharedSlice<"image_gallery", ImageGallerySliceVariation>;
/**
 * Primary content in LeftContentBlock → Primary
 *
 */
interface LeftContentBlockSliceDefaultPrimary {
    /**
     * Title field in *LeftContentBlock → Primary*
     *
     * - **Field Type**: Title
     * - **Placeholder**: This is where it all begins...
     * - **API ID Path**: left_content_block.primary.title
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    title: prismicT.TitleField;
    /**
     * Description field in *LeftContentBlock → Primary*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: A nice description of your feature
     * - **API ID Path**: left_content_block.primary.description
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    description: prismicT.RichTextField;
    /**
     * image field in *LeftContentBlock → Primary*
     *
     * - **Field Type**: Image
     * - **Placeholder**: *None*
     * - **API ID Path**: left_content_block.primary.image
     * - **Documentation**: https://prismic.io/docs/core-concepts/image
     *
     */
    image: prismicT.ImageField<never>;
}
/**
 * Default variation for LeftContentBlock Slice
 *
 * - **API ID**: `default`
 * - **Description**: `LeftContentBlock`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type LeftContentBlockSliceDefault = prismicT.SharedSliceVariation<"default", Simplify<LeftContentBlockSliceDefaultPrimary>, never>;
/**
 * Slice variation for *LeftContentBlock*
 *
 */
type LeftContentBlockSliceVariation = LeftContentBlockSliceDefault;
/**
 * LeftContentBlock Shared Slice
 *
 * - **API ID**: `left_content_block`
 * - **Description**: `LeftContentBlock`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type LeftContentBlockSlice = prismicT.SharedSlice<"left_content_block", LeftContentBlockSliceVariation>;
/**
 * Primary content in RightContentBlock → Primary
 *
 */
interface RightContentBlockSliceDefaultPrimary {
    /**
     * Title field in *RightContentBlock → Primary*
     *
     * - **Field Type**: Title
     * - **Placeholder**: This is where it all begins...
     * - **API ID Path**: right_content_block.primary.title
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    title: prismicT.TitleField;
    /**
     * Description field in *RightContentBlock → Primary*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: A nice description of your feature
     * - **API ID Path**: right_content_block.primary.description
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    description: prismicT.RichTextField;
    /**
     * image field in *RightContentBlock → Primary*
     *
     * - **Field Type**: Image
     * - **Placeholder**: *None*
     * - **API ID Path**: right_content_block.primary.image
     * - **Documentation**: https://prismic.io/docs/core-concepts/image
     *
     */
    image: prismicT.ImageField<never>;
}
/**
 * Default variation for RightContentBlock Slice
 *
 * - **API ID**: `default`
 * - **Description**: `RightContentBlock`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type RightContentBlockSliceDefault = prismicT.SharedSliceVariation<"default", Simplify<RightContentBlockSliceDefaultPrimary>, never>;
/**
 * Slice variation for *RightContentBlock*
 *
 */
type RightContentBlockSliceVariation = RightContentBlockSliceDefault;
/**
 * RightContentBlock Shared Slice
 *
 * - **API ID**: `right_content_block`
 * - **Description**: `RightContentBlock`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type RightContentBlockSlice = prismicT.SharedSlice<"right_content_block", RightContentBlockSliceVariation>;
/**
 * Primary content in SingleColumnText → Primary
 *
 */
interface SingleColumnTextSliceDefaultPrimary {
    /**
     * Title field in *SingleColumnText → Primary*
     *
     * - **Field Type**: Title
     * - **Placeholder**: This is where it all begins...
     * - **API ID Path**: single_column_text.primary.title
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    title: prismicT.TitleField;
    /**
     * Description field in *SingleColumnText → Primary*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: A nice description of your feature
     * - **API ID Path**: single_column_text.primary.description
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    description: prismicT.RichTextField;
}
/**
 * Default variation for SingleColumnText Slice
 *
 * - **API ID**: `default`
 * - **Description**: `SingleColumnText`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type SingleColumnTextSliceDefault = prismicT.SharedSliceVariation<"default", Simplify<SingleColumnTextSliceDefaultPrimary>, never>;
/**
 * Slice variation for *SingleColumnText*
 *
 */
type SingleColumnTextSliceVariation = SingleColumnTextSliceDefault;
/**
 * SingleColumnText Shared Slice
 *
 * - **API ID**: `single_column_text`
 * - **Description**: `SingleColumnText`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type SingleColumnTextSlice = prismicT.SharedSlice<"single_column_text", SingleColumnTextSliceVariation>;
/**
 * Primary content in TopBannerSection → Primary
 *
 */
interface TopBannerSectionSliceDefaultPrimary {
    /**
     * Title field in *TopBannerSection → Primary*
     *
     * - **Field Type**: Title
     * - **Placeholder**: This is where it all begins...
     * - **API ID Path**: top_banner_section.primary.title
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    title: prismicT.TitleField;
    /**
     * Body field in *TopBannerSection → Primary*
     *
     * - **Field Type**: Rich Text
     * - **Placeholder**: A nice description of your feature
     * - **API ID Path**: top_banner_section.primary.body
     * - **Documentation**: https://prismic.io/docs/core-concepts/rich-text-title
     *
     */
    body: prismicT.RichTextField;
    /**
     * Background Image field in *TopBannerSection → Primary*
     *
     * - **Field Type**: Image
     * - **Placeholder**: *None*
     * - **API ID Path**: top_banner_section.primary.background_image
     * - **Documentation**: https://prismic.io/docs/core-concepts/image
     *
     */
    background_image: prismicT.ImageField<"mobile">;
}
/**
 * Default variation for TopBannerSection Slice
 *
 * - **API ID**: `default`
 * - **Description**: `TopBannerSection`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type TopBannerSectionSliceDefault = prismicT.SharedSliceVariation<"default", Simplify<TopBannerSectionSliceDefaultPrimary>, never>;
/**
 * Slice variation for *TopBannerSection*
 *
 */
type TopBannerSectionSliceVariation = TopBannerSectionSliceDefault;
/**
 * TopBannerSection Shared Slice
 *
 * - **API ID**: `top_banner_section`
 * - **Description**: `TopBannerSection`
 * - **Documentation**: https://prismic.io/docs/core-concepts/reusing-slices
 *
 */
export type TopBannerSectionSlice = prismicT.SharedSlice<"top_banner_section", TopBannerSectionSliceVariation>;
declare module "@prismicio/client" {
    interface CreateClient {
        (repositoryNameOrEndpoint: string, options?: prismic.ClientConfig): prismic.Client<AllDocumentTypes>;
    }
    namespace Content {
        export type { BlogListDocumentData, BlogListDocument, BlogPageDocumentData, BlogPageDocumentDataSlicesSlice, BlogPageDocument, CmsPageDocumentData, CmsPageDocumentDataSlicesSlice, CmsPageDocument, HomepageDocumentData, HomepageDocumentDataSlicesSlice, HomepageDocument, AllDocumentTypes, CenteredTextSliceDefaultPrimary, CenteredTextSliceDefault, CenteredTextSliceVariation, CenteredTextSlice, DoubleColumnTextSliceDefaultPrimary, DoubleColumnTextSliceDefault, DoubleColumnTextSliceVariation, DoubleColumnTextSlice, ImageGallerySliceDefaultPrimary, ImageGallerySliceDefaultItem, ImageGallerySliceDefault, ImageGallerySliceVariation, ImageGallerySlice, LeftContentBlockSliceDefaultPrimary, LeftContentBlockSliceDefault, LeftContentBlockSliceVariation, LeftContentBlockSlice, RightContentBlockSliceDefaultPrimary, RightContentBlockSliceDefault, RightContentBlockSliceVariation, RightContentBlockSlice, SingleColumnTextSliceDefaultPrimary, SingleColumnTextSliceDefault, SingleColumnTextSliceVariation, SingleColumnTextSlice, TopBannerSectionSliceDefaultPrimary, TopBannerSectionSliceDefault, TopBannerSectionSliceVariation, TopBannerSectionSlice };
    }
}
