import MyComponent from '../../../../slices/TopBannerSection';

export default {
  title: 'slices/TopBannerSection'
}


export const _Default = () => <MyComponent slice={{"variation":"default","version":"sktwi1xtmkfgx8626","items":[{}],"primary":{"title":[{"type":"heading1","text":"Teach","spans":[]}],"body":[{"type":"paragraph","text":"Qui minim elit et irure est fugiat tempor anim commodo minim veniam amet. Eiusmod laboris cupidatat do est ullamco in dolor nisi proident irure anim tempor.","spans":[]},{"type":"paragraph","text":"Et consequat exercitation officia est proident tempor sit tempor excepteur anim ad proident. Id sunt exercitation irure aliquip anim commodo.","spans":[]}],"background_image":{"dimensions":{"width":900,"height":500},"alt":null,"copyright":null,"url":"https://images.unsplash.com/photo-1495476479092-6ece1898a101","mobile":{"dimensions":{"width":390,"height":844},"alt":null,"copyright":null,"url":"https://images.unsplash.com/photo-1593642633279-1796119d5482"}}},"slice_type":"top_banner_section","id":"_Default"}} />
_Default.storyName = ''
