import MyComponent from '../../../../slices/RightContentBlock';

export default {
  title: 'slices/RightContentBlock'
}


export const _Default = () => <MyComponent slice={{"variation":"default","version":"sktwi1xtmkfgx8626","items":[{}],"primary":{"title":[{"type":"heading1","text":"Please","spans":[]}],"description":[{"type":"paragraph","text":"Aliqua duis mollit ipsum ea tempor eiusmod est reprehenderit sunt excepteur officia in. Non veniam esse non cupidatat dolor sunt consequat exercitation. Dolore nulla exercitation minim enim id esse est laboris sint sit labore id.","spans":[]}],"image":{"dimensions":{"width":900,"height":500},"alt":null,"copyright":null,"url":"https://images.unsplash.com/photo-1494438639946-1ebd1d20bf85"}},"slice_type":"right_content_block","id":"_Default"}} />
_Default.storyName = ''
