import MyComponent from '../../../../slices/DoubleColumnText';

export default {
  title: 'slices/DoubleColumnText'
}


export const _Default = () => <MyComponent slice={{"variation":"default","version":"sktwi1xtmkfgx8626","items":[{}],"primary":{"title":[{"type":"heading1","text":"Diagram","spans":[]}],"description":[{"type":"paragraph","text":"Minim est irure est quis reprehenderit veniam sit reprehenderit anim sint velit dolor.","spans":[]},{"type":"paragraph","text":"Nulla sit enim minim dolor consequat cillum in consectetur nostrud deserunt magna ad irure duis velit. Excepteur qui ad nostrud anim amet consequat veniam ipsum. Qui aliquip labore irure.","spans":[]},{"type":"paragraph","text":"Duis enim incididunt do in ex excepteur sunt commodo adipisicing nisi.","spans":[]},{"type":"paragraph","text":"Nulla quis sunt nulla pariatur ea nulla incididunt dolor cillum. Ad ullamco est id velit ex ea amet mollit non.","spans":[]}]},"slice_type":"double_column_text","id":"_Default"}} />
_Default.storyName = ''
