import MyComponent from '../../../../slices/LeftContentBlock';

export default {
  title: 'slices/LeftContentBlock'
}


export const _Default = () => <MyComponent slice={{"variation":"default","version":"sktwi1xtmkfgx8626","items":[{}],"primary":{"title":[{"type":"heading2","text":"Cannot","spans":[]}],"description":[{"type":"paragraph","text":"Minim tempor nostrud sint incididunt proident dolore pariatur proident.","spans":[]},{"type":"paragraph","text":"Ea Lorem enim aliqua labore culpa adipisicing aliquip nulla ad laborum pariatur.","spans":[]}],"image":{"dimensions":{"width":900,"height":500},"alt":null,"copyright":null,"url":"https://images.unsplash.com/photo-1494438639946-1ebd1d20bf85"}},"slice_type":"left_content_block","id":"_Default"}} />
_Default.storyName = ''
