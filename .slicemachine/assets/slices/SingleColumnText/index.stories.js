import MyComponent from '../../../../slices/SingleColumnText';

export default {
  title: 'slices/SingleColumnText'
}


export const _Default = () => <MyComponent slice={{"variation":"default","version":"sktwi1xtmkfgx8626","items":[{}],"primary":{"title":[{"type":"heading1","text":"Describe","spans":[]}],"description":[{"type":"paragraph","text":"Commodo amet Lorem fugiat ad Lorem dolore reprehenderit irure eiusmod adipisicing. Id reprehenderit in occaecat excepteur.","spans":[]},{"type":"paragraph","text":"Consectetur officia mollit est nostrud aute enim exercitation cupidatat amet dolore aliqua laborum in.","spans":[]},{"type":"paragraph","text":"Amet ut eu fugiat irure cillum ullamco eiusmod exercitation non dolor do tempor.","spans":[]}]},"slice_type":"single_column_text","id":"_Default"}} />
_Default.storyName = ''
