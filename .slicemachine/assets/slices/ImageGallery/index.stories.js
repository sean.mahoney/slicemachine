import MyComponent from '../../../../slices/ImageGallery';

export default {
  title: 'slices/ImageGallery'
}


export const _Default = () => <MyComponent slice={{"variation":"default","version":"sktwi1xtmkfgx8626","items":[{"gallery_image":{"dimensions":{"width":900,"height":500},"alt":null,"copyright":null,"url":"https://images.unsplash.com/photo-1534759926787-89fa60f35848"}}],"primary":{"title":[{"type":"heading1","text":"Mother","spans":[]}]},"slice_type":"image_gallery","id":"_Default"}} />
_Default.storyName = ''
